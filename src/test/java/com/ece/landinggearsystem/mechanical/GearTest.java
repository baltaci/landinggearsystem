/**
 * 
 */
package com.ece.landinggearsystem.mechanical;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ece.landinggearsystem.mechanical.Gear;
import com.ece.landinggearsystem.mechanical.Gear.GearStatus;
import com.ece.landinggearsystem.mechanical.Gear.LatchingBoxStatus;

/**
 * @author baltaci
 *
 */
public class GearTest {

	@Test
	public void initialGearStatus() {
		Gear gear = new Gear("Test Gear");
		assertEquals("Gears should be initially down", gear.getGearStatus(), GearStatus.DOWN);	
	}
	
	@Test
	public void initialGearUpLatchingBox() {
		Gear gear = new Gear("Test Gear");
		assertEquals("Up Latching Box should be UNLOCKED", gear.getUpLatchingBoxStatus(), LatchingBoxStatus.UNLOCKED);	
	}
	
	@Test
	public void initialGearDownLatchingBox() {
		Gear gear = new Gear("Test Gear");
		assertEquals("Down Latching Box should be LOCKED", gear.getDownLatchingBoxStatus(), LatchingBoxStatus.LOCKED);	
	}
	
	@Test
	public void gearRetraction() throws InterruptedException {
		Gear gear = new Gear("Test Gear");
		gear.retract();
		assertEquals("Down Latching Box should be UNLOCKED", gear.getDownLatchingBoxStatus(), LatchingBoxStatus.UNLOCKED);
		assertEquals("Up Latching Box should be LOCKED", gear.getUpLatchingBoxStatus(), LatchingBoxStatus.LOCKED);	
		assertEquals("Gears should be finally up", gear.getGearStatus(), GearStatus.UP);	
	}
	
	@Test
	public void gearExtension() throws InterruptedException {
		Gear gear = new Gear("Test Gear");
		gear.extend();
		assertEquals("Down Latching Box should be LOCKED", gear.getDownLatchingBoxStatus(), LatchingBoxStatus.LOCKED);
		assertEquals("Up Latching Box should be UNLOCKED", gear.getUpLatchingBoxStatus(), LatchingBoxStatus.UNLOCKED);	
		assertEquals("Gears should be finally down", gear.getGearStatus(), GearStatus.DOWN);	
	}
}
