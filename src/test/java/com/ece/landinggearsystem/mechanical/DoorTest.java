package com.ece.landinggearsystem.mechanical;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ece.landinggearsystem.mechanical.Door.DoorStatus;
import com.ece.landinggearsystem.mechanical.Door.LatchingBoxStatus;

public class DoorTest {

	@Test
	public void initialGearStatus() {
		Door door = new Door("Test Door");
		assertEquals("Door should be initially opened", door.getDoorStatus(), DoorStatus.OPEN);	
	}
	
	@Test
	public void initialFirstLatchingBoxStatus() {
		Door door = new Door("Test Door");
		assertEquals("First Latching Box should be initially UNLOCKED", door.getFirstLatchingBoxStatus(), LatchingBoxStatus.UNLOCKED);	
	}
	
	@Test
	public void initialSecondLatchingBoxStatus() {
		Door door = new Door("Test Door");
		assertEquals("Second Latching Box should be initially UNLOCKED", door.getSecondLatchingBoxStatus(), LatchingBoxStatus.UNLOCKED);	
	}
}
