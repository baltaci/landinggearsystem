/**
 * 
 */
package com.ece.landinggearsystem.pilotinterface;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author baltaci
 *
 */
public class EmergencyButton extends JPanel {
	
	private static ImageIcon normal = new ImageIcon("src/main/resources/images/normal.png");
	private JButton emergencyButton;
	
	public EmergencyButton(){
		emergencyButton = new JButton(normal);
		emergencyButton.setBorder(BorderFactory.createTitledBorder("Emergency Button"));
		emergencyButton.setPreferredSize(new Dimension(150,370));
		add(emergencyButton);
	}

	public JButton getEmergencyButton() {return emergencyButton;}
	public void setEmergencyButton(JButton emergencyButton) {this.emergencyButton = emergencyButton;}
	
}
