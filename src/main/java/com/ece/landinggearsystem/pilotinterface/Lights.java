package com.ece.landinggearsystem.pilotinterface;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author baltaci
 *
 */
public class Lights extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private static ImageIcon nolight = new ImageIcon("src/main/resources/images/no_light.png");
	private static ImageIcon greenlight = new ImageIcon("src/main/resources/images/green_light.png");
	private static ImageIcon orangelight = new ImageIcon("src/main/resources/images/yellow_light.png");
	private static ImageIcon redlight = new ImageIcon("src/main/resources/images/red_light.png");

	private JLabel front = new JLabel("Front", nolight, JLabel.CENTER);
	private JLabel right = new JLabel("Right", nolight, JLabel.LEFT);
	private JLabel left = new JLabel("Left", nolight, JLabel.LEFT);
	
	public Lights(){
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder("Health Lights"));
		setPreferredSize(new Dimension(300,360));
		add(front, BorderLayout.NORTH);
		add(right, BorderLayout.EAST);
		add(left, BorderLayout.WEST);
		
	}

	/**
	 * @return the front
	 */
	public JLabel getFront() {
		return front;
	}

	/**
	 * @param front the front to set
	 */
	public void setFront(JLabel front) {
		this.front = front;
	}
	
	/**
	 * @return the greenlight
	 */
	public static ImageIcon getGreenlight() {
		return greenlight;
	}

	/**
	 * @param greenlight the greenlight to set
	 */
	public static void setGreenlight(ImageIcon greenlight) {
		Lights.greenlight = greenlight;
	}
	

	/**
	 * @return the redlight
	 */
	public static ImageIcon getRedlight() {
		return redlight;
	}

	/**
	 * @param redlight the redlight to set
	 */
	public static void setRedlight(ImageIcon redlight) {
		Lights.redlight = redlight;
	}
	
	/**
	 * @return the nolight
	 */
	public static ImageIcon getNolight() {
		return nolight;
	}

	/**
	 * @param nolight the nolight to set
	 */
	public static void setNolight(ImageIcon nolight) {
		Lights.nolight = nolight;
	}

	/**
	 * @return the orangelight
	 */
	public static ImageIcon getOrangelight() {
		return orangelight;
	}

	/**
	 * @param orangelight the orangelight to set
	 */
	public static void setOrangelight(ImageIcon orangelight) {
		Lights.orangelight = orangelight;
	}

	/**
	 * @return the right
	 */
	public JLabel getRight() {
		return right;
	}

	/**
	 * @param right the right to set
	 */
	public void setRight(JLabel right) {
		this.right = right;
	}

	/**
	 * @return the left
	 */
	public JLabel getLeft() {
		return left;
	}

	/**
	 * @param left the left to set
	 */
	public void setLeft(JLabel left) {
		this.left = left;
	}
	
}
