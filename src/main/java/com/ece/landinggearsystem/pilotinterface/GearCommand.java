package com.ece.landinggearsystem.pilotinterface;

import java.awt.Dimension;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author baltaci
 *
 */
public class GearCommand extends JPanel implements Observer{
	
	private JSlider command;
	
	public GearCommand(){
		command = new JSlider(JSlider.VERTICAL, 0, 1, 0);
		//command.addChangeListener((ChangeListener) this);

		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(new Integer( 0 ), new JLabel("DOWN"));
		labelTable.put(new Integer(1), new JLabel("UP"));
		command.setLabelTable(labelTable);
		command.setPaintLabels(true);
		command.setMajorTickSpacing(1);
		command.setMinorTickSpacing(1);
		command.setSnapToTicks(true);
		command.setBorder(BorderFactory.createTitledBorder("Landing Gear"));
		command.setPreferredSize(new Dimension(150,370));
		add(command);
	}

	public void update(Observable o, Object arg) {
		
	}
	
	/**
	 * @return the command
	 */
	public JSlider getCommand() {return command;}

	/**
	 * @param command the command to set
	 */
	public void setCommand(JSlider command) {this.command = command;}

}
