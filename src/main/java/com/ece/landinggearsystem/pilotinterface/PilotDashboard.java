package com.ece.landinggearsystem.pilotinterface;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author baltaci
 *
 */
public class PilotDashboard extends JFrame {

	private GearCommand gc = new GearCommand();
	private Lights leds = new Lights();
	private EmergencyButton emgb = new EmergencyButton();

	public PilotDashboard(){
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(gc, BorderLayout.WEST);
		panel.add(leds, BorderLayout.CENTER);
		panel.add(emgb, BorderLayout.EAST);
		
		setTitle("Pilot Interface");
		setSize(800,400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(panel);
		this.setVisible(true);
	}		

	public GearCommand getGearCommand(){return gc;}
	public Lights getLights(){return leds;}
	public EmergencyButton getEmergencyButton(){return emgb;}

}
