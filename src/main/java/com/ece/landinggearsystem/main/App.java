package com.ece.landinggearsystem.main;

import com.ece.landinggearsystem.digital.ComputingModule;
import com.ece.landinggearsystem.mechanical.ElectroValve;

/**
 * Main !
 *
 */
public class App 
{
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		ComputingModule cm = new ComputingModule();
	}
}