package com.ece.landinggearsystem.mechanical;

import java.util.Observable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ElectroValve extends Observable{

	private String name;
	private Double hydraulicIn;
	private Double hydraulicOut;
	private boolean electricalPort;

	public ElectroValve(String name){
		this.name = name;
		electricalPort = false;
		hydraulicIn = 100.0;
		hydraulicOut = 0.0;
	}

	public void electricalOrder(Boolean electricalPort) {
		this.electricalPort = electricalPort;
		ScheduledExecutorService thread = Executors.newSingleThreadScheduledExecutor();
		if(this.electricalPort){
			ElectroValveClosing evc = new ElectroValveClosing();
			while(hydraulicOut < 100){
				thread.scheduleAtFixedRate(evc, 0, 100, TimeUnit.MILLISECONDS);
				setChanged();
				notifyObservers(this);
				System.out.println("ElectroValve ("+this.name+") Closing Out / Pressure Level %: "+ hydraulicOut);
			}thread.shutdownNow();	
		}
		else {
			ElectroValveOpening evo = new ElectroValveOpening();
			while(hydraulicOut > 0){
				thread.scheduleAtFixedRate(evo, 0, 100, TimeUnit.MILLISECONDS);
				setChanged();
				notifyObservers(this);
				System.out.println("ElectroValve ("+this.name+") Opening Out / Pressure Level % : "+ hydraulicOut);
			}thread.shutdownNow();

		}
	}

	public Double getHydraulicIn() {return hydraulicIn;}
	public void setHydraulicIn(Double hydraulicIn) {this.hydraulicIn = hydraulicIn;}
	public Double getHydraulicOut() {return hydraulicOut;}
	public void setHydraulicOut(Double hydraulicOut) {this.hydraulicOut = hydraulicOut;}
	public boolean isElectricalPort() {return electricalPort;}
	public void setElectricalPort(boolean electricalPort) {this.electricalPort = electricalPort;}
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

	class ElectroValveClosing implements Runnable{
		public void run() {
			if(hydraulicOut < 100)
				hydraulicOut += hydraulicIn*0.1;
		}
	}

	class ElectroValveOpening implements Runnable{
		public void run(){
			if(hydraulicOut > 0)
				hydraulicOut -= hydraulicIn*0.1;
		}
	}
}

