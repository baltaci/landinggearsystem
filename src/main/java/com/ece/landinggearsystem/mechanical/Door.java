/**
 * 
 */
package com.ece.landinggearsystem.mechanical;

import java.util.Observable;
import java.util.Observer;

import com.ece.landinggearsystem.mechanical.Cylinder.CylinderStatus;

/**
 * @author baltaci
 *
 */
public class Door implements Observer{

	public enum DoorStatus{OPEN, CLOSE, OPENING, CLOSING}
	public enum LatchingBoxStatus{LOCKED, UNLOCKED}
	private DoorStatus doorStatus;
	private LatchingBoxStatus firstLatchingBoxStatus;
	private LatchingBoxStatus secondLatchingBoxStatus;
	private String name;

	public Door(String name){
		this.name = name;
		setFirstLatchingBoxStatus(LatchingBoxStatus.UNLOCKED);
		setSecondLatchingBoxStatus(LatchingBoxStatus.UNLOCKED);
		setDoorStatus(DoorStatus.OPEN);
	}
	
	public void opening(){
		setFirstLatchingBoxStatus(LatchingBoxStatus.UNLOCKED);
		setSecondLatchingBoxStatus(LatchingBoxStatus.UNLOCKED);
		setDoorStatus(DoorStatus.OPEN);
	}

	public void closing(){
		setDoorStatus(DoorStatus.CLOSE);
		setFirstLatchingBoxStatus(LatchingBoxStatus.LOCKED);
		setSecondLatchingBoxStatus(LatchingBoxStatus.LOCKED);
	}
	
	public void update(Observable o, Object arg) {
		if (arg instanceof CylinderStatus) {
			CylinderStatus cs = (CylinderStatus) arg;
			
			if(cs == CylinderStatus.HIGH){
				closing();
			} 
			else if(cs == CylinderStatus.DOWN){
				opening();
			} 
			else if (cs == CylinderStatus.STUCK){
				
			}
		}
	}

	public LatchingBoxStatus getFirstLatchingBoxStatus() {
		return firstLatchingBoxStatus;
	}

	public void setFirstLatchingBoxStatus(LatchingBoxStatus firstLatchingBoxStatus) {
		this.firstLatchingBoxStatus = firstLatchingBoxStatus;
	}

	public LatchingBoxStatus getSecondLatchingBoxStatus() {
		return secondLatchingBoxStatus;
	}

	public void setSecondLatchingBoxStatus(LatchingBoxStatus secondLatchingBoxStatus) {
		this.secondLatchingBoxStatus = secondLatchingBoxStatus;
	}

	public DoorStatus getDoorStatus() {
		return doorStatus;
	}

	public void setDoorStatus(DoorStatus doorStatus) {
		this.doorStatus = doorStatus;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}

