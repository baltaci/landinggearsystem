/**
 * 
 */
package com.ece.landinggearsystem.mechanical;

/**
 * @author baltaci
 *
 */
public class Sensor {

	private float speed;
	private float force;
	public static final float planeWeight = 1000000;

	public Sensor(){
		setSpeed(0);
		setForce(-planeWeight);			
	}

	public float getSpeed(){return this.speed;}
	public float getForce(){return this.force;}	
	public void setSpeed(float speed){this.speed = speed;}
	public void setForce(float force){this.force = force;}	
}
