/**
 * 
 */
package com.ece.landinggearsystem.mechanical;

import java.util.Observable;
import java.util.Observer;


/**
 * @author baltaci
 *
 */
public class Cylinder extends Observable implements Observer{

	public enum CylinderStatus{HIGH, DOWN, STUCK}
	CylinderStatus cylinderStatus;
	private Double hydraulicPressureRetraction;
	private Double hydraulicPressureExtension;
	private String name;
	
	public Cylinder(String name, ElectroValve aEV, ElectroValve bEV){
		this.name = name;
		this.hydraulicPressureRetraction = aEV.getHydraulicOut();
		this.hydraulicPressureExtension = bEV.getHydraulicOut();
		compare();
	}

	public void compare(){
		if(hydraulicPressureRetraction > hydraulicPressureExtension){
			cylinderStatus = CylinderStatus.DOWN;
		} else if(hydraulicPressureRetraction < hydraulicPressureExtension){
			cylinderStatus = CylinderStatus.HIGH;
		}else{
			cylinderStatus = CylinderStatus.STUCK;
		}
		setChanged();
		notifyObservers(this.cylinderStatus);
	}
	
	public void update(Observable o, Object arg) {
		if (arg instanceof ElectroValve) {
			ElectroValve ev = (ElectroValve) arg;
			if(ev.getName().equals("closeDoorsEV")){
				hydraulicPressureRetraction = ev.getHydraulicOut();
				compare();
				System.out.println(this.name+" Status ("+ev.getName()+") : "+ cylinderStatus);
			}
			else if(ev.getName().equals("openDoorsEV")){
				hydraulicPressureExtension = ev.getHydraulicOut();
				compare();
				System.out.println(this.name+" Status ("+ev.getName()+") : "+ cylinderStatus);
			}
			else if(ev.getName().equals("retractGearsEV")){
				hydraulicPressureRetraction = ev.getHydraulicOut();
				compare();
				System.out.println(this.name+" Status ("+ev.getName()+") : "+ cylinderStatus);
			}
			else if(ev.getName().equals("extendGearsEV")){
				hydraulicPressureExtension = ev.getHydraulicOut();
				compare();
				System.out.println(this.name+" Status ("+ev.getName()+") : "+ cylinderStatus);
			}	
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
