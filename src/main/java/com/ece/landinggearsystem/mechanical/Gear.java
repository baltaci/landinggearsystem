/**
 * 
 */
package com.ece.landinggearsystem.mechanical;

import java.util.Observable;
import java.util.Observer;

import com.ece.landinggearsystem.mechanical.Cylinder.CylinderStatus;

/**
 * @author baltaci
 *
 */
public class Gear extends Observable implements Observer{

	public enum GearStatus{UP, DOWN, goUP, goDOWN, STUCK}
	public enum LatchingBoxStatus{LOCKED, UNLOCKED}
	private GearStatus gearStatus;
	private LatchingBoxStatus upLatchingBoxStatus;
	private LatchingBoxStatus downLatchingBoxStatus;
	private String name;

	public Gear(String name){
		this.name = name;
		this.setGearStatus(GearStatus.DOWN);
		this.setDownLatchingBoxStatus(LatchingBoxStatus.LOCKED);
		this.setUpLatchingBoxStatus(LatchingBoxStatus.UNLOCKED);
	}

	public void retract() throws InterruptedException{
		setDownLatchingBoxStatus(LatchingBoxStatus.UNLOCKED);
		setGearStatus(GearStatus.goUP);
		//Thread.sleep(200);
		setGearStatus(GearStatus.UP);
		setUpLatchingBoxStatus(LatchingBoxStatus.LOCKED);
	}
	
	public void extend() throws InterruptedException{
		setUpLatchingBoxStatus(LatchingBoxStatus.UNLOCKED);
		setGearStatus(GearStatus.goDOWN);
		//Thread.sleep(200);
		setGearStatus(GearStatus.DOWN);
		setDownLatchingBoxStatus(LatchingBoxStatus.LOCKED);
	}
	
	public void update(Observable o, Object arg) {
		if (arg instanceof CylinderStatus) {
			CylinderStatus cs = (CylinderStatus) arg;
			
			if(cs == CylinderStatus.HIGH){
				try {
					retract();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			else if(cs == CylinderStatus.DOWN){
				try {
					extend();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			else if (cs == CylinderStatus.STUCK){
				
			}
			setChanged();
			notifyObservers(this);
		}
		
	}

	public LatchingBoxStatus getUpLatchingBoxStatus() {
		return upLatchingBoxStatus;
	}

	public void setUpLatchingBoxStatus(LatchingBoxStatus upLatchingBoxStatus) {
		this.upLatchingBoxStatus = upLatchingBoxStatus;
	}

	public LatchingBoxStatus getDownLatchingBoxStatus() {
		return downLatchingBoxStatus;
	}

	public void setDownLatchingBoxStatus(LatchingBoxStatus downLatchingBoxStatus) {
		this.downLatchingBoxStatus = downLatchingBoxStatus;
	}

	public GearStatus getGearStatus() {
		return gearStatus;
	}

	public void setGearStatus(GearStatus gearStatus) {
		this.gearStatus = gearStatus;
		setChanged();
		notifyObservers(this);
	}		
	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}

