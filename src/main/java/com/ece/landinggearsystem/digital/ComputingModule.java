package com.ece.landinggearsystem.digital;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.ece.landinggearsystem.mechanical.Cylinder;
import com.ece.landinggearsystem.mechanical.Door;
import com.ece.landinggearsystem.mechanical.ElectroValve;
import com.ece.landinggearsystem.mechanical.Gear;
import com.ece.landinggearsystem.mechanical.Gear.GearStatus;
import com.ece.landinggearsystem.pilotinterface.Lights;
import com.ece.landinggearsystem.pilotinterface.PilotDashboard;

public class ComputingModule implements Observer {



	ElectroValve generalEV = new ElectroValve("generalEV");
	ElectroValve closeDoorsEV = new ElectroValve("closeDoorsEV");
	ElectroValve openDoorsEV = new ElectroValve("openDoorsEV");
	ElectroValve retractGearsEV = new ElectroValve("retractGearsEV");
	ElectroValve extendGearsEV = new ElectroValve("extendGearsEV");

	Cylinder frontDoorCylinder = new Cylinder("Froot Door Cylinder", closeDoorsEV, openDoorsEV);
	Cylinder rightDoorCylinder = new Cylinder("Right Door Cylinder", closeDoorsEV, openDoorsEV);
	Cylinder leftDoorCylinder = new Cylinder("Left Door Cylinder", closeDoorsEV, openDoorsEV);

	Cylinder frontGearCylinder = new Cylinder("Front Gear Cylinder", retractGearsEV, extendGearsEV);
	Cylinder rightGearCylinder = new Cylinder("Right Gear Cylinder", retractGearsEV, extendGearsEV);
	Cylinder leftGearCylinder = new Cylinder("Left Gear Cylinder", retractGearsEV, extendGearsEV);

	Gear frontGear = new Gear("Front Gear");
	Gear rightGear = new Gear("Right Gear");
	Gear leftGear = new Gear("Left Gear");

	Door frontDoor = new Door("Front Door");
	Door rightDoor = new Door("Right Door");
	Door leftDoor = new Door("Left Door");
	
	PilotDashboard dashboard = new PilotDashboard();

	public ComputingModule(){
		frontGear.addObserver(this);
		rightGear.addObserver(this);
		leftGear.addObserver(this);

		frontGearCylinder.addObserver(frontGear);
		rightGearCylinder.addObserver(rightGear);
		leftGearCylinder.addObserver(leftGear);

		frontDoorCylinder.addObserver(frontDoor);
		rightDoorCylinder.addObserver(rightDoor);
		leftDoorCylinder.addObserver(leftDoor);

		closeDoorsEV.addObserver(frontDoorCylinder);
		closeDoorsEV.addObserver(rightDoorCylinder);
		closeDoorsEV.addObserver(leftDoorCylinder);

		openDoorsEV.addObserver(frontDoorCylinder);
		openDoorsEV.addObserver(rightDoorCylinder);
		openDoorsEV.addObserver(leftDoorCylinder);

		retractGearsEV.addObserver(frontGearCylinder);
		retractGearsEV.addObserver(rightGearCylinder);
		retractGearsEV.addObserver(leftGearCylinder);

		extendGearsEV.addObserver(frontGearCylinder);
		extendGearsEV.addObserver(rightGearCylinder);
		extendGearsEV.addObserver(leftGearCylinder);

		dashboard.getGearCommand().getCommand().addChangeListener(new HandleChangeListener());
		//front.addObserver(dashboard.getGearCommand());	
	}

	class HandleChangeListener implements ChangeListener {
		public void stateChanged(ChangeEvent changeEvent) {
			Object source = changeEvent.getSource();
			if (source instanceof JSlider) {
				JSlider aModel = (JSlider) source;
				if (!aModel.getValueIsAdjusting()) {
					System.out.println("Changed: " + aModel.getValue());

					//Handle Down
					if(aModel.getValue() == 0){
						generalEV.electricalOrder(true);
						closeDoorsEV.electricalOrder(false);
						openDoorsEV.electricalOrder(true);
						retractGearsEV.electricalOrder(false);
						extendGearsEV.electricalOrder(true);
					}
					//Handle Up
					else {
						generalEV.electricalOrder(true);
						openDoorsEV.electricalOrder(false);
						closeDoorsEV.electricalOrder(true);
						extendGearsEV.electricalOrder(false);
						retractGearsEV.electricalOrder(true);
					}
				}
			} else {
				System.out.println("Something changed: " + source);
			}
		}
	}

	public void update(Observable o, Object arg) {
		if (arg instanceof Gear) {
			Gear gear = (Gear) arg;
			if(gear.getName().equals("Front Gear")){
				if(gear.getGearStatus() == GearStatus.DOWN){
					dashboard.getLights().getFront().setIcon(Lights.getGreenlight());
				} else if(gear.getGearStatus() == GearStatus.UP){
					dashboard.getLights().getFront().setIcon(Lights.getNolight());
				}
				else if (gear.getGearStatus() == GearStatus.goUP || gear.getGearStatus() == GearStatus.goDOWN){
					dashboard.getLights().getRight().setIcon(Lights.getOrangelight());
				}
				else {
					dashboard.getLights().getRight().setIcon(Lights.getRedlight());
				}
			}
			else if (gear.getName().equals("Right Gear")){
				if(gear.getGearStatus() == GearStatus.DOWN){
					dashboard.getLights().getRight().setIcon(Lights.getGreenlight());
				} else if(gear.getGearStatus() == GearStatus.UP){
					dashboard.getLights().getRight().setIcon(Lights.getNolight());
				}
				else if (gear.getGearStatus() == GearStatus.goUP || gear.getGearStatus() == GearStatus.goDOWN){
					dashboard.getLights().getRight().setIcon(Lights.getOrangelight());
				}
				else {
					dashboard.getLights().getRight().setIcon(Lights.getRedlight());
				}
			}
			else if(gear.getName().equals("Left Gear")){
				if(gear.getGearStatus() == GearStatus.DOWN){
					dashboard.getLights().getLeft().setIcon(Lights.getGreenlight());
				} else if(gear.getGearStatus() == GearStatus.UP){
					dashboard.getLights().getLeft().setIcon(Lights.getNolight());
				}
				else if (gear.getGearStatus() == GearStatus.goUP || gear.getGearStatus() == GearStatus.goDOWN){
					dashboard.getLights().getRight().setIcon(Lights.getOrangelight());
				}
				else {
					dashboard.getLights().getRight().setIcon(Lights.getRedlight());
				}
			}
			
		}
	}
}